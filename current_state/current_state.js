/**
 * @file
 * Javascript behaviors to handle the workflow state.
 */


(function($) {
Drupal.behaviors.current_state= {
    attach : function(context, settings) {
      jQuery(document).ready(function($) {
      var refreshId = setInterval(function(){
         // do ajax request
         jQuery.ajax({
            type : "GET",
            url : Drupal.settings.baseurl.path+'/ajax/displayblock',
            dataType : "html",
            success : function(data) {
			// append the ajax response
            jQuery('#workflow_state_id').empty().append(data);
            }
        });
       },20000);
      });
    }
};
}(jQuery));