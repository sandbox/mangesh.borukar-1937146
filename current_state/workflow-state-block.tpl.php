<?php

/**
 * @file
 * Default theme implementation for rendering workflow state in block.
 *
 * Available variables:
 * - $workflow_state: Array of Node with workflow state
 *
 * @see template_preprocess_workflow_state_block()
 *
 * @ingroup themeable
 */
?>
<div id='workflow_state_id'>
<?php foreach ($workflow_state as $state):?>
  <div><?php print l($state['title']."( ".$state['state']." )",'node/'.$state['nid'].'/edit'); ?></div>  
<?php endforeach; ?>
</div>